import logging
import time
import gitlab
import conf as cf

PROJECTS = []
LAST_COMMITS = {}
conf = cf.asdict()
token = conf['GENERAL']['gitlab_token']
urls_file = conf['APPLICATION']['git_urls_file']
timeout = int(conf['APPLICATION']['timeout_between_listening'])
urls = open(urls_file, 'r+').read().split(',')
logging.basicConfig(filename='logs.log')

git = gitlab.Gitlab(private_token=token)
git.auth()


def save_git_projects():
    global PROJECTS, LAST_COMMITS
    print('Get projects and save last commit')
    for url in urls:
        try:
            project = git.projects.get(url)
            PROJECTS.append(project)
            last_commit = project.commits.list()[0]
            LAST_COMMITS[project.name] = last_commit
            print(f'Found project {project.name}')
        except Exception as e:
            logging.error(f'Error occurred when fetching project {url}: {e.__class__.__name__}')


def get_last_commit(project_path):
    project = git.projects.get(project_path)
    last_commit = project.commits.list()[0]
    return last_commit


def is_new_commit(project):
    last_known_commit = LAST_COMMITS[project.name]
    last_commit = get_last_commit(project.path_with_namespace)
    return not last_known_commit.id == last_commit.id


def is_syntax(string):
    return "%issue:#" in string


def get_issue_number(string):
    first_sep = '%issue:#'
    second_sep = "%"
    return int(string[string.index(first_sep, 0) + len(first_sep):string.index(second_sep, 1)])


def close_issue_with_string(string, project, commit):
    try:
        issue_number = get_issue_number(string)
        issue = project.issues.get(issue_number)
        issue.state_event = 'close'
        issue.save()
        logging.warning(f'Issue #{issue_number} closed by {commit.committer_name} ({commit.committer_email}) at {commit.committed_date} ({commit.web_url})')
    except Exception as e:
        logging.error(f'Failed to close an issue (commit {commit.web_url}): {e.__class__.__name__}')


def listen_for_commits():
    global LAST_COMMITS
    projects_string = PROJECTS[0].name + ''.join([f', {project.name}' for project in PROJECTS[1:]])
    print(f'Now listening for new commit on {projects_string}')
    running = True
    while running:
        time.sleep(timeout)
        for project in PROJECTS:
            if is_new_commit(project):
                new_commit = get_last_commit(project.path_with_namespace)
                LAST_COMMITS[project.name] = new_commit
                if is_syntax(new_commit.title):
                    close_issue_with_string(new_commit.title, project, new_commit)
                elif is_syntax(new_commit.message):
                    close_issue_with_string(new_commit.message, project, new_commit)
                else:
                    pass


if __name__ == '__main__':
    print('Program starting...')
    save_git_projects()
    listen_for_commits()
